const dimensions = {
  TINY: 2,
  SMALL: 4,
  MEDIUM: 8,
  LARGE: 16,
  XL: 24,
};

const fontSizes = {
  // global sizes
  DISPLAY4: 112,
  DISPLAY3: 56,
  DISPLAY2: 45,
  DISPLAY1: 34,
  HEADLINE: 28,
  TITLE: 20,
  SUBTITLE: 18,
  BASE: 16,
  BODY: 14,
  CAPTION: 12,
  SMALL: 10,
};

const colors = {
  //Primary color
  PRIMARY: '#0088cf',
  PRIMARY_DARK: '#005b9e',
  PRIMARY_LIGHT: '#5db8ff',
  BG_CONTENT: '#dddddd',
  TEXTBE: '#00003b',
  TERTIARY: '#FFE358',
  ACCENT: '#F3534A',

  SEMI_LIGHT_GRAY: '#ededed',
  PLACEHOLDER: '#D0D0D0',
  LIGHT_GRAY: '#fafafa',
  GRAY: '#BDBFC7',
  GRAY2: '#D8D8D8',
  GRAY3: '#F0F0F0',
  GRAY4: '#F7F8FA',
  DARK_GRAY: '#323643',

  COLOR_INFO: '#5bc0de',
  COLOR_SUCCESS: '#5CC5A7',
  COLOR_WARNING: '#f0ad4e',
  COLOR_ERROR: '#d9534f',
  BORDER: '#8c8c8c',

  INFO: '#5bc0de',
  SUCCESS: '#5CC5A7',
  WARNING: '#f0ad4e',
  ERROR: '#d9534f',
  WHITE: '#ffffff',

  WHITE_BLUR: '#fff6',
  BLACK: '#323643',
  TRANSPARENT: 'transparent',

  WORD: '#2699FB',
  BGITEM: '#F7F7FA',
  BORDERINPUT: '#BCE0FD',
  BGRINPUT: '#F1F9FF',
  BGRCAMERA: '#8AC8FC',
  BGSUCCESSFUL: '#2A2E43',
  BUTTON_BACK: '#FC5F8A',
};

export {dimensions, fontSizes, colors};
