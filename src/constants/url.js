const baseUrl = 'http://doanhnhansg.mpoint.vn/';
const prefix = 'app/thanhvd';
const endpoint = {
  LOGIN: 'api/user/login-qr',
  REFRESH_TOKEN: 'api/user/refresh-token',
  GET_INFORMATION: 'api/news/get-all',
  GET_BANNER: 'api/banner/get-all',
  GET_COMPANY: 'api/company/get-all',
  GET_COMPANY_DETAIL: 'api/company/get-detail',
  UPDATE_PROFILE: 'api/user/update-profile',
  UPLOAD_IMAGE: 'api/file/upload-image',
};

export {baseUrl, endpoint, prefix};
