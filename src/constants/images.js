const images = {
  ICON_CENTER: require('../assets/icons/iconCenterTabar.png'),
  LOGO_DNSG: require('../assets/images/logoDNSG.png'),
  HEADER_LOGIN: require('../assets/images/headerLogin.jpg'),
  CARD_DEMO: require('../assets/images/cardDemo.png'),
  BG_PROFILE: require('../assets/images/pgProfile.jpg'),
  CONTACT: require('../assets/images/user.png'),
  CARD: require('../assets/images/card.png'),
  ICONBG: require('../assets/images/pgCircle.png'),
  ICONLOA: require('../assets/icons/caiLoa.png'),
  ICONHOITHAO: require('../assets/icons/thangNguoi.png'),
  ICONNHOM: require('../assets/icons/tronNguoi.png'),
  NEWTAG: require('../assets/images/newTag.png'),
  BGUSER: require('../assets/images/user-panel.png'),
};

export default images;
