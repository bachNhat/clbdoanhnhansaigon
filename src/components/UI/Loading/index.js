
import React, { Component } from "react";
import { View, TouchableOpacity, Text, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { colors } from "../../../constants/theme";
import styles from './styles'
import {
  SkypeIndicator,
} from 'react-native-indicators'
class Loading extends Component {
  render() {
    if (this.props.showLoading) {
      return (
        <View
          style={styles.container}
        >
          <View style={styles.wrapLoading}>
            <View style={{ height: 50 }}>
              <SkypeIndicator count={5} size={50} color={colors.PRIMARY} />
            </View>
          </View>

        </View>
      );
    } else {
      return null;
    }
  }
}

mapStateToProps = state => {
  return {
    showLoading: state.uiState.showLoading
  };
};

export default connect(mapStateToProps)(Loading);
