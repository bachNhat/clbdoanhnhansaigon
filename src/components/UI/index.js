import React, {Component} from 'react';
import Loading from './Loading';
import Message from './Message';
import ConfirmBox from './ConfirmBox';
import {View} from 'react-native';

export default class UIContainer extends Component {
  render() {
    return (
      <View style={{position: 'absolute'}}>
        <Loading />
        <Message />
        <ConfirmBox />
      </View>
    );
  }
}
