import React, {Component} from 'react';
import {Text, View, TextInput} from 'react-native';
import Block from '../../components/block';
import {
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
} from 'native-base';
import {withNavigation} from 'react-navigation';
import styles from './styles';

class HeaderC extends Component {
  render() {
    const {title, official, back, input, navigation} = this.props;
    return (
      <Header style={styles.customHeader}>
        {official ? (
          <>
            <Left style={styles.leftOFF}>
              <Button transparent onPress={() => alert('hahah')}>
                <Icon name="pin" style={styles.icon} />
              </Button>
            </Left>
            <Left style={styles.leftOFF}>
              <Button
                transparent
                onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')
                }>
                <Icon name="notifications" style={styles.icon} />
              </Button>
            </Left>
            <Right style={styles.rightOFF}>
              <TextInput
                style={styles.inputHeader}
                placeholder="Tìm kiếm"
                onChangeText={text => input(text)}
              />
              <Icon name="search" style={styles.iconSearch} />
            </Right>
          </>
        ) : (
          <>
            {back ? (
              <Left style={{flex: 1}}>
                <Button transparent onPress={() => navigation.pop()}>
                  <Icon name="arrow-back" style={styles.iconBack} />
                </Button>
              </Left>
            ) : (
              <Left />
            )}
            <Body style={{flex: 5}}>
              <Title style={styles.title}>{title}</Title>
            </Body>
            <Right style={{flex: 1}} />
          </>
        )}
      </Header>
    );
  }
}
export default withNavigation(HeaderC);
