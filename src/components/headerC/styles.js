import {StyleSheet, Platform} from 'react-native';
import {colors} from '../../constants/theme';

const styles = StyleSheet.create({
  title: {
    fontSize: 17,
    fontWeight: '500',
    color: 'white',
  },
  customHeader: {
    backgroundColor: colors.PRIMARY,
    height: Platform.OS === 'ios' ? 80 : 60,
  },
  headerOFF: {},
  leftOFF: {
    // backgroundColor: 'gray',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyOFF: {
    // backgroundColor: 'tomato',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightOFF: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  inputHeader: {
    height: 40,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 20,
    paddingLeft: 40,
  },
  icon: {
    color: 'white',
  },
  iconSearch: {
    position: 'absolute',
    left: 10,
    color: 'gray',
  },
  iconBack: {
    color: 'white',
    marginLeft: 10,
  },
});

export default styles;
