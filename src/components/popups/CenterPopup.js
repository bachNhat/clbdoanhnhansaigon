import React from "react";
import { Modal, View, Dimensions, KeyboardAvoidingView } from "react-native";
const { width, height } = Dimensions.get('window')

const CenterPopup = ({ children, visible }) => {
  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
    <Modal animationType="fade" transparent visible={visible}>
      <View
        style={[{
          // justifyContent: "center",
          alignItems: "center",
          paddingTop:200,
          marginBottom:50,
          height: height,
          width: width,
          backgroundColor: "rgba(0, 0, 0, 0.6)",
          zIndex:1
        }]}
      >
        {children}
      </View>
    </Modal>
    </KeyboardAvoidingView>
  );
};

export default CenterPopup;
