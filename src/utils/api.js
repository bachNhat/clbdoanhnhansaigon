import axios from 'axios';
import {baseUrl, endpoint, prefix} from '../constants/url';
import getUniqueString from './getUniqueString';
import storeT from '../utils/store';
import moment from 'moment';
import helpers from '../globals/helpers';
import RNFetchBlob from 'react-native-fetch-blob';
axios.defaults.baseURL = baseUrl;
//setToken
export const setAuthToken = token => {
  // console.log(token);
  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
};
//Login
export async function login({token}) {
  console.log(token);
  try {
    const res = await axios.post(`/${endpoint.LOGIN}`, {
      token,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}
//RefreshToken
export async function refreshToken() {
  try {
    const res = await axios.post(`/${endpoint.REFRESH_TOKEN}`, {});
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}
//Tin tức
export async function getTinTuc({skip, limit, genres}) {
  console.log(skip, limit, genres);
  try {
    const res = await axios.post(`/${endpoint.GET_INFORMATION}`, {
      skip,
      limit,
      genres,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}
//Banner
export async function banner() {
  try {
    const res = await axios.post(`/${endpoint.GET_BANNER}`);
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

//getCompanyAll
export async function getAllCompany({skip, limit}) {
  console.log(skip, limit);
  try {
    const res = await axios.post(`/${endpoint.GET_COMPANY}`, {
      skip,
      limit,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

//update profile
export async function updateProfile({
  name,
  email,
  identification,
  birth,
  phone,
  gender,
  address,
  avatar,
}) {
  try {
    const res = await axios.post(`/${endpoint.UPDATE_PROFILE}`, {
      name,
      email,
      identification,
      birth,
      phone,
      gender,
      address,
      avatar,
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}

//update profile
export async function updateImages({image}) {
  console.log(image);
  let file = {
    uri: image.path,
    type: image.mime,
    name: 'file.jpg',
  };
  let data = new FormData();
  data.append('file', file);
  try {
    const res = await axios.post(`/${endpoint.UPLOAD_IMAGE}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return res.data;
  } catch (error) {
    if (error.response) {
      console.log(error.response);
      helpers.showMessage(
        error.response.data.message || 'Lỗi hệ thống.Vui lòng thử lại sau',
      );
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}
