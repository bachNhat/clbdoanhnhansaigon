import {createStackNavigator} from 'react-navigation-stack';

import Start from '../screens/Start/StartScreen';
import Login from '../screens/Login/LoginScreen';

const StackScreens = createStackNavigator(
  {
    // Start,
    Login,
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

export default StackScreens;
