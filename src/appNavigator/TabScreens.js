import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {Icon} from 'native-base';
import React, {Component} from 'react';
import {colors} from '../constants/theme';
import InformationScreen from '../screens/Information/InformationScreen';
import GiftExchangeScreen from '../screens/GiftExchange/GiftExchangeScreen';
import UserScreen from '../screens/User/UserScreen';
import HomeScreen from '../screens/Home/HomeScreen';
import Block from '../components/block';
import {View, Platform, Image} from 'react-native';
import images from '../constants/images';
import CameraScanScreen from '../screens/CameraScan/CameraScanScreen';

const TabScreens = createBottomTabNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'Trang chủ',
      },
    },
    InformationScreen: {
      screen: InformationScreen,
      navigationOptions: {
        tabBarLabel: 'Tin tức',
      },
    },
    CameraScanScreen: {
      screen: CameraScanScreen,
      navigationOptions: {
        tabBarLabel: 'Camera',
        tabBarIcon: ({focused, horizontal, tintColor}) => (
          <Block center middle>
            <Image
              resizeMode="contain"
              style={{
                width: 60,
                height: 60,
                marginBottom: 30,
              }}
              source={images.ICON_CENTER}
            />
          </Block>
        ),
      },
    },
    GiftExchangeScreen: {
      screen: GiftExchangeScreen,
      navigationOptions: {
        tabBarLabel: 'Đổi quà',
      },
    },
    UserScreen: {
      screen: UserScreen,
      navigationOptions: {
        tabBarLabel: 'Tài khoản',
      },
    },
  },
  {
    initialRouteName: 'HomeScreen',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'HomeScreen') {
          iconName = 'home';
        } else if (routeName === 'InformationScreen') {
          iconName = 'bookmark';
        } else if (routeName === 'GiftExchangeScreen') {
          iconName = 'gift';
        } else if (routeName === 'UserScreen') {
          iconName = 'contact';
        }
        return <Icon name={iconName} size={25} style={{color: tintColor}} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: colors.PRIMARY,
      inactiveTintColor: 'gray',
      tabStyle: {},
      style: {
        borderTopWidth: 0,
      },
      labelStyle: {},
    },
  },
);

export default TabScreens;
