import {createStackNavigator} from 'react-navigation-stack';
import StackScreens from './StackScreens';
// import DrawerScreens from './DrawerScreens';
import TabScreens from './TabScreens';
import CameraScanScreen from '../screens/CameraScan/CameraScanScreen';
import FellowshipScreen from '../screens/Fellowship/FellowshipScreen';
import TradeScreen from '../screens/Trade/TradeScreen';
import headerC from '../components/headerC';
import NotificationScreen from '../screens/Notification/NotificationScreen';
import EventScreen from '../screens/Event/EventScreen';
import ContentInformationScreen from '../screens/ContentInformation/ContentInformationScreen';

const AppNavigator = createStackNavigator(
  {
    StackScreens: {
      screen: StackScreens,
      navigationOptions: {
        header: null,
      },
    },
    TabScreens: {
      screen: TabScreens,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      },
    },
    CameraScanScreen: {
      screen: CameraScanScreen,
      navigationOptions: {
        header: null,
      },
    },
    FellowshipScreen: {
      screen: FellowshipScreen,
      navigationOptions: {
        header: null,
      },
    },
    TradeScreen: {
      screen: TradeScreen,
      navigationOptions: {
        header: null,
      },
    },
    NotificationScreen: {
      screen: NotificationScreen,
      navigationOptions: {
        header: null,
      },
    },
    EventScreen: {
      screen: EventScreen,
      navigationOptions: {
        header: null,
      },
    },
    ContentInformationScreen: {
      screen: ContentInformationScreen,
      navigationOptions: {
        header: null,
      },
    },
    // Invoice,
    // Phone,
    // EditUserLink,
    // DataCard,
    // Camera,
    // CodeDetails
  },
  {
    initialRouteName: 'StackScreens',
  },
);

export default AppNavigator;
