let store = null

let helpers = {
    setStore: (newstore) => {
        store = newstore
    },
    showLoading: () => {
        store.dispatch({ type: "SHOW_LOADING" });
    },
    hideLoading: () => {
        store.dispatch({ type: "HIDE_LOADING" });
    },
    showMessage: (msg, title, btMsg, funcMsg) => {
        if (title == null) {
            title = "Thông báo";
        }
        store.dispatch({
            type: "SHOW_MESSAGE",
            content: msg,
            title: title,
            btMsg,
            funcMsg
        });
    },
    hideMessageBox: () => {
        store.dispatch({ type: "HIDE_MESSAGE" });
    },
    showConfirm: (content, title, titlebntOK, titlebntHuy, btnOK, btnHuy) => {
        store.dispatch({
            type: "SHOW_CONFIRM_BOX",
            content,
            title,
            titlebntOK,
            titlebntHuy,
            btnOK,
            btnHuy
        });
    },
    hideConfirm: () => {
        store.dispatch({
            type: "HIDE_CONFIRM_BOX"
        });
    },

}

export default helpers