import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer, {rootSaga} from './state';

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = middleware => {
  if (__DEV__) {
    const {logger} = require('redux-logger');
    return applyMiddleware(...middleware, logger);
  }
  return applyMiddleware(...middleware);
};

function configureStore(initialState = {}) {
  const store = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware]),
  );

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
  };

  store.runSagaTask();
  return store;
}

export default configureStore;
