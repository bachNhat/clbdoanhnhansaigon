import * as Types from './types';

export const loginUser = data => {
  return {
    type: Types.LOGIN,
    payload: data,
  };
};

export const refreshToken = payload => {
  return {
    type: Types.REFRESH_TOKEN,
    payload,
  };
};

export const information = payload => {
  return {
    type: Types.INFOMATION,
    payload,
  };
};

export const banner = payload => {
  return {
    type: Types.GET_BANNER,
    payload,
  };
};

export const company = payload => {
  return {
    type: Types.GET_COMPANY,
    payload,
  };
};

export const companyDetail = payload => {
  return {
    type: Types.GET_COMPANY_DETAIL,
    payload,
  };
};

export const updateProfile = payload => {
  return {
    type: Types.UPDATE_PROFILE,
    payload,
  };
};

export const uploadImage = payload => {
  return {
    type: Types.UPLOAD_IMAGE,
    payload,
  };
};
