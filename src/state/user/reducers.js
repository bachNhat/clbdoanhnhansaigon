import * as Types from './types';

const initialState = {
  profile: [],
  tokenMpoit: '',
  profileUpdate: [],
  banner: [],
  informationRD: [],
  allCompanyRD: [],
  isRefreshing: true,
  eventRD: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        profile: action.payload,
      };
    case Types.TOKEN_MPOINT:
      return {
        ...state,
        tokenMpoit: action.payload,
      };
    case Types.GET_BANNER_SUCCESS:
      return {
        ...state,
        banner: action.payload.data,
      };
    case Types.INFOMATION_SUCCESS:
      return {
        ...state,
        informationRD: action.payload.data,
      };
    case Types.EVENT_SUCCESS:
      return {
        ...state,
        eventRD: action.payload.data,
      };
    case Types.GET_COMPANY_SUCCESS:
      return {
        ...state,
        allCompanyRD: state.allCompanyRD.concat(action.payload.data),
      };
    case Types.UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        profileUpdate: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
