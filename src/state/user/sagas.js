import {call, put, takeLatest, all} from 'redux-saga/effects';
import {
  login,
  setAuthToken,
  refreshToken,
  getTinTuc,
  banner,
  getAllCompany,
  updateProfile,
  updateImages,
} from '../../utils/api';
import * as Types from './types';
import storeT from '../../utils/store';
import {sleep} from '../../utils/utilities';
import {Alert} from 'react-native';
import helpers from '../../globals/helpers';
import {NavigationActions, StackActions} from 'react-navigation';
import {storeData, getData} from '../../asyncStorage/AsynStorage';
import * as TypesAS from '../../asyncStorage/Types';
import {red} from 'ansi-colors';

//LoginSaga
function* userLoginSaga(action) {
  console.log(action);
  try {
    helpers.showLoading();
    const loginUser = yield call(login, action.payload);
    console.log(loginUser);
    //getBanner
    const getBanner = yield call(banner, action.payload);
    if (getBanner.code == 0) {
      yield put({
        type: Types.GET_BANNER_SUCCESS,
        payload: getBanner,
      });
    } else helpers.showMessage('Lỗi get banner');
    /////////////
    if (loginUser.code == 0) {
      yield put({
        type: Types.LOGIN_SUCCESS,
        payload: loginUser.data.userInfo,
      });
      yield put({
        type: Types.TOKEN_MPOINT,
        payload: loginUser.data.mpointToken,
      });
      storeData(
        TypesAS.SET_TOKEN_LOGIN,
        JSON.stringify(loginUser.data.tokenJwt),
      );
      storeData(
        TypesAS.SET_TOKEN_MPOINT,
        JSON.stringify(loginUser.data.mpointToken.token.token),
      );
      yield setAuthToken(loginUser.data.tokenJwt);
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'TabScreens'})],
      });
      action.payload.success.dispatch(resetAction);
      // yield call()
    }
    helpers.hideLoading();
  } catch (e) {
    console.log(e);
    helpers.hideLoading();
  }
}

export function* watchUserLogin() {
  yield takeLatest(Types.LOGIN, userLoginSaga);
}

//refreshTokenSaga
function* refreshTokenUser(action) {
  try {
    helpers.showLoading();
    yield setAuthToken(action.payload.token);
    const loginUser = yield call(refreshToken);
    console.log(loginUser);
    //getBanner
    const getBanner = yield call(banner, action.payload);
    if (getBanner.code == 0) {
      yield put({
        type: Types.GET_BANNER_SUCCESS,
        payload: getBanner,
      });
    } else helpers.showMessage('Lỗi get banner');
    //////////
    if (loginUser.code == 0) {
      yield put({
        type: Types.LOGIN_SUCCESS,
        payload: loginUser.userInfo,
      });
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'TabScreens'})],
      });
      action.payload.success.dispatch(resetAction);
    }
    helpers.hideLoading();
  } catch (error) {
    console.log(error);
    helpers.hideLoading();
  }
}

export function* watchrefreshTokenUser() {
  yield takeLatest(Types.REFRESH_TOKEN, refreshTokenUser);
}

//GetInfomationSaga
function* getInformation(action) {
  try {
    helpers.showLoading();
    const res = yield call(getTinTuc, action.payload);
    console.log(res);
    if (res.message == 'Thành công') {
      // helpers.showMessage('tin tuc thanh cong');
      if (action.payload.genres == 1) {
        yield put({
          type: Types.INFOMATION_SUCCESS,
          payload: res,
        });
      } else if (action.payload.genres == 2) {
        yield put({
          type: Types.EVENT_SUCCESS,
          payload: res,
        });
      }
    }
    helpers.hideLoading();
  } catch (error) {
    console.log(error);
    helpers.hideLoading();
  }
}

export function* watchGetInformation() {
  yield takeLatest(Types.INFOMATION, getInformation);
}

//GetCompanySaga
function* getCompany(action) {
  console.log(action);
  try {
    helpers.showLoading();
    const res = yield call(getAllCompany, action.payload);
    console.log(res);
    if (res.message == 'Thành công') {
      // helpers.showMessage('tin tuc thanh cong');
      yield put({
        type: Types.GET_COMPANY_SUCCESS,
        payload: res,
      });
    }
    helpers.hideLoading();
  } catch (error) {
    console.log(error);
    helpers.hideLoading();
  }
}

export function* watchCompany() {
  yield takeLatest(Types.GET_COMPANY, getCompany);
}

//Update Profile
function* updatePRfile(action) {
  console.log(action);
  try {
    helpers.showLoading();
    const res = yield call(updateProfile, action.payload);
    console.log(res);
    // console.log(res);
    if (res.code == 0) {
      helpers.showMessage(res.message);
      yield put({
        type: Types.UPDATE_PROFILE_SUCCESS,
        payload: res,
      });
    }
    helpers.hideLoading();
  } catch (error) {
    console.log(error);
    helpers.hideLoading();
  }
}

export function* watchUpdatePRfile() {
  yield takeLatest(Types.UPDATE_PROFILE, updatePRfile);
}

//upload image
function* upImage(action) {
  console.log(action);
  try {
    helpers.showLoading();
    const res = yield call(updateImages, action.payload);
    console.log(res);
    // if (res.code == 0) {
    //   helpers.showMessage(res.message);
    //   yield put({
    //     type: Types.UPDATE_PROFILE_SUCCESS,
    //     payload: res,
    //   });
    // }
    helpers.hideLoading();
  } catch (error) {
    console.log(error);
    helpers.hideLoading();
  }
}

export function* watchUpImage() {
  yield takeLatest(Types.UPLOAD_IMAGE, upImage);
}
