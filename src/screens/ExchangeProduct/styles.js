import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    height,
    backgroundColor: '#dddddd',
  },
  itemContent: {
    width: '32.4%',
    backgroundColor: 'white',
    marginRight: 5,
    marginBottom: 5,
    // paddingVertical: 15,
    borderRadius: 8,
  },
  img: {
    width: '100%',
    height: 130,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    // paddingVertical: 10,
  },
  itemName: {
    // backgroundColor: 'blue',
    color: 'white',
    fontSize: 12,
  },
  viewName: {
    backgroundColor: colors.BORDER,
    paddingVertical: 5,
    borderBottomRightRadius: 8,
    borderBottomLeftRadius: 8,
  },
  viewImage: {
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
});

export default styles;
