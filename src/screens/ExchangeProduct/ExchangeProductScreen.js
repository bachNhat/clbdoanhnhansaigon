import React, {useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';
import Block from '../../components/block';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Picker,
  Form,
} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';

const ExchangeProductScreen = () => {
  const data = [
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
    {
      name: 'Made in Japan',
      urlImg:
        'https://nongdan.pro/wp-content/uploads/2017/06/made-in-japan.jpg',
    },
  ];

  return (
    <Container>
      <Content>
        <Block flex={1} style={styles.content}>
          <FlatList
            data={data}
            keyExtractor={item => item.id + ''}
            numColumns={3}
            // horizontal={true}
            // ListHeaderComponent={renderHeaderFlatlist}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity style={styles.itemContent}>
                <Block style={styles.viewImage}>
                  <Image
                    resizeMode="contain"
                    style={styles.img}
                    source={{uri: item.urlImg}}
                  />
                </Block>
                <Block center middle style={styles.viewName}>
                  <Text style={styles.itemName}>{item.name}</Text>
                </Block>
              </TouchableOpacity>
            )}
          />
        </Block>
      </Content>
    </Container>
  );
};

export default ExchangeProductScreen;
