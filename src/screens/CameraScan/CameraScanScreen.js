import React, {Component} from 'react';
import {Text, View, Dimensions, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';
const {width, height} = Dimensions.get('window');
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import storeT from '../../utils/store';

class CameraScanScreen extends Component {
  componentDidMount() {
    // setTimeout(() => {
    //   ;
    // }, 3000);
  }

  reciepCode = false;
  onBarCodeRead = data => {
    const {login} = this.props;
    if (this.reciepCode) return;
    if (data) {
      console.log(data.data);
      this.reciepCode = true;
      const param = this.props.navigation.getParam('from', '');
      switch (param) {
        case 'loginScreen':
          login({token: data.data, success: this.props.navigation});
          break;
        default:
        // code block
      }
    }
    setTimeout(() => (this.reciepCode = false), 5000);
  };
  render() {
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{flex: 1}}
        captureAudio={false}
        type={RNCamera.Constants.Type.back}
        flashMode={RNCamera.Constants.FlashMode.off}
        onBarCodeRead={this.onBarCodeRead}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        <View
          style={{
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.5)',
            height: height / 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontSize: 15}}>
            Di chuyển camera tới vùng chứa mã để quét{' '}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: height / 3,
            flexDirection: 'row',
          }}>
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.5)',
              width: (width - height / 3) / 2,
            }}
          />
          <View
            style={{
              width: height / 3,
            }}
          />
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.5)',
              width: (width - height / 3) / 2,
            }}
          />
        </View>
        <View
          style={{
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.5)',
            height: height / 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.pop();
            }}
            style={{
              backgroundColor: 'gray',
              justifyContent: 'center',
              alignItems: 'center',
              width: width / 3,
              height: 40,
              borderRadius: 5,
            }}>
            <Text style={{color: '#fff'}}>Đóng</Text>
          </TouchableOpacity>
        </View>
      </RNCamera>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      login: userActions.loginUser,
      refreshtoken: userActions.refreshToken,
    },
    dispatch,
  );

export default connect(
  null,
  mapDispatchToProps,
)(CameraScanScreen);
