import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  bgUser: {
    width: '100%',
    height: 200,
    borderRadius: 8,
  },
  avatarUser: {
    width: 90,
    height: 90,
    backgroundColor: 'red',
    borderRadius: 50,
    marginTop: 10,
  },
  point: {
    fontSize: 16,
    fontWeight: '600',
    color: colors.PRIMARY,
    marginTop: 20,
  },
  touchPoint: {
    paddingVertical: 5,
    paddingHorizontal: 25,
    backgroundColor: colors.PRIMARY,
    borderRadius: 8,
    marginTop: 5,
  },
  card: {
    width: '90%',
    height: 140,
  },
  viewBlue: {
    backgroundColor: colors.PRIMARY,
    height: 40,
    marginVertical: 5,
    borderRadius: 8,
  },
  viewProfile: {
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 5,
  },
  company: {
    textAlign: 'center',
    marginBottom: 5,
  },
  viewCompany: {
    backgroundColor: 'white',
    borderBottomColor: 'gray',
    borderBottomWidth: 0.3,
    height: 40,
  },
  textItem: {
    marginLeft: 10,
  },
  textInput: {
    width: '90%',
    marginLeft: 10,
    height: 45,
  },
  iconItem: {
    fontSize: 25,
    color: colors.PRIMARY_DARK,
    width: 25,
  },
  viewBG: {
    borderRadius: 8,
  },
  viewSubmit: {
    paddingVertical: 10,
    paddingHorizontal: 50,
    backgroundColor: colors.PRIMARY,
    marginVertical: 20,
    borderRadius: 8,
  },
});

export default styles;
