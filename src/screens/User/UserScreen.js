import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput,
  Dimensions,
  Alert,
} from 'react-native';
import Block from '../../components/block';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Picker,
  Form,
  ListItem,
  CheckBox,
} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import location from '../../constants/data/locations.json';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import ImagePicker from 'react-native-image-crop-picker';
const {width, height} = Dimensions.get('window');

const UserScreen = ({profile, updateProfile, uploadImage}) => {
  const [showBidthDay, setShowBidthDay] = useState(false);
  const [bidthDay, setBidthDay] = useState(moment(profile.birth).format('L'));
  const [gender, setGender] = useState(profile.gender == 1 ? true : false);
  const [identification, setIdentification] = useState(profile.identification);
  const [address, setAddress] = useState(profile.address);
  const [email, setEmail] = useState(profile.email);
  const [phone, setPhone] = useState(profile.phone);
  const [name, setName] = useState(profile.name);
  const [avatar, setAvatar] = useState('');

  const handleDatePicked = date => {
    setBidthDay(date);
    setShowBidthDay(false);
  };

  const onsupmit = () => {
    updateProfile({
      name,
      email,
      identification,
      birth: moment(bidthDay).valueOf(),
      phone,
      gender: gender ? 1 : 2,
      address,
      avatar,
    });
  };

  const camera = () => {
    ImagePicker.openCamera({
      // mediaType: 'video',
    }).then(image => {
      console.log(image);
      uploadImage({image});
    });
  };
  const liblary = () => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      uploadImage({image});
    });
  };

  const openCamera1 = () => {
    Alert.alert(
      'Thông báo',
      'Bạn muốn lấy ảnh từ camera hay thư viện !',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Camera', onPress: camera},
        {text: 'Thư viện ảnh', onPress: liblary},
      ],
      {cancelable: false},
    );
  };

  // useEffect(() => {
  //   if (props.startTime !== startTime) {
  //     setStartTime(props.startTime);
  //   }
  // }, []);
  return (
    <Container>
      <HeaderC title="THÔNG TIN TÀI KHOẢN" />
      <Content style={{padding: 5, backgroundColor: colors.BG_CONTENT}}>
        <Block style={styles.viewBG}>
          <ImageBackground
            source={images.BGUSER}
            style={styles.bgUser}
            resizeMode="cover">
            <Block row>
              <Block flex={1} center middle>
                <TouchableOpacity
                  style={styles.avatarUser}
                  onPress={openCamera1}
                />
                {profile.point ? (
                  <>
                    <Text style={styles.point}>{profile.point}</Text>
                    <Text>Điểm mPoint</Text>
                  </>
                ) : null}

                <TouchableOpacity style={styles.touchPoint}>
                  <Text style={{color: 'white'}}>Đổi điểm</Text>
                </TouchableOpacity>
              </Block>
              <Block top flex={1}>
                <Image
                  source={images.CARD}
                  resizeMode="contain"
                  style={styles.card}
                />
              </Block>
            </Block>
          </ImageBackground>
        </Block>
        <Block style={styles.viewBlue} />
        <Block style={styles.viewProfile}>
          {profile.companyInfo ? (
            <Block center middle style={styles.viewCompany}>
              <Text style={styles.company}>{profile.companyInfo.name}</Text>
            </Block>
          ) : null}
          <Block middle style={styles.viewCompany}>
            <Block row center>
              <Icon name="contact" style={styles.iconItem} />
              <TextInput
                style={styles.textInput}
                placeholder="Họ và tên"
                onChangeText={text => setName(text)}
                value={name}
              />
            </Block>
          </Block>
          <Block middle style={styles.viewCompany}>
            <TouchableOpacity onPress={() => setShowBidthDay(true)}>
              <Block row center>
                <Icon name="calendar" style={styles.iconItem} />
                <Text style={styles.textItem}>Ngày sinh</Text>
                {bidthDay ? (
                  <Text style={styles.textItem}>
                    {moment(bidthDay).format('DD/MM/YYYY')}
                  </Text>
                ) : null}
              </Block>
            </TouchableOpacity>
          </Block>
          <DateTimePicker
            isVisible={showBidthDay}
            onConfirm={handleDatePicked}
            onCancel={() => setShowBidthDay(false)}
          />
          <Block middle style={styles.viewCompany}>
            <Block row center>
              <Icon name="contacts" style={styles.iconItem} />
              <ListItem
                style={{marginLeft: 10}}
                onPress={() => setGender(true)}>
                <CheckBox checked={gender} />
                <Text style={styles.textItem}>Nam</Text>
              </ListItem>
              <ListItem onPress={() => setGender(false)}>
                <CheckBox checked={!gender} />
                <Text style={styles.textItem}>Nữ</Text>
              </ListItem>
            </Block>
          </Block>
          <Block middle style={styles.viewCompany}>
            <Block row center>
              <Icon name="call" style={styles.iconItem} />
              <TextInput
                style={styles.textInput}
                placeholder="Số điện thoại"
                keyboardType="numeric"
                onChangeText={text => setPhone(text)}
                value={phone}
              />
            </Block>
          </Block>
          <Block middle style={styles.viewCompany}>
            <Block row center>
              <Icon name="mail" style={styles.iconItem} />
              <TextInput
                style={styles.textInput}
                placeholder="Email"
                onChangeText={text => setEmail(text)}
                value={email}
              />
            </Block>
          </Block>
          <Block middle style={styles.viewCompany}>
            <Block row center>
              <Icon name="home" style={styles.iconItem} />
              <TextInput
                style={styles.textInput}
                placeholder="Địa chỉ"
                onChangeText={text => setAddress(text)}
                value={address}
              />
            </Block>
          </Block>
          <Block middle style={[styles.viewCompany]}>
            <Block row center>
              <Icon name="card" style={styles.iconItem} />
              <TextInput
                style={styles.textInput}
                placeholder="Số CMTNN"
                keyboardType="numeric"
                onChangeText={text => setIdentification(text)}
                value={identification}
              />
            </Block>
          </Block>
          {!name ? (
            <Block center middle>
              <Text style={{color: 'red'}}>
                Họ và tên không được để trống !
              </Text>
            </Block>
          ) : null}
          <Block center middle>
            {!name ? (
              <TouchableOpacity
                style={[styles.viewSubmit, {backgroundColor: colors.GRAY}]}>
                <Text style={{color: 'white'}}>Xác Nhận</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.viewSubmit} onPress={onsupmit}>
                <Text style={{color: 'white'}}>Xác Nhận</Text>
              </TouchableOpacity>
            )}
          </Block>
        </Block>

        <Block style={{height: 50}} />
      </Content>
    </Container>
  );
};

const mapStateToProps = state => ({
  profile: state.user.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateProfile: userActions.updateProfile,
      uploadImage: userActions.uploadImage,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserScreen);
