import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    // height,
  },
  contact: {
    width: 80,
    height: 80,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  lineGRa: {
    width: '100%',
    aspectRatio: 986 / 441,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    color: 'white',
    fontWeight: '600',
    fontSize: 12,
  },
  card: {
    width: '95%',
    height: 120,
  },
  touchntc: {
    backgroundColor: '#6394ae',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginTop: 10,
  },
  bgProfile: {
    width: '100%',
    aspectRatio: 35 / 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tab: {
    borderColor: colors.DARK_GRAY,
    borderWidth: 1,
    width: '100%',
    // height: (11 * width) / 35,
    // aspectRatio: 35 / 11,
    marginTop: 10,
    borderRadius: 8,
    padding: 5,
  },
  iconreward: {
    width: '50%',
    height: 40,
  },
  viewFlat: {
    marginRight: 30,
  },
  imageFlat: {
    width: 300,
    height: 170,
    borderRadius: 8,
  },
  viewFlatHome: {
    marginTop: 30,
  },
});

export default styles;
