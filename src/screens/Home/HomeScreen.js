import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StatusBar,
} from 'react-native';
import Block from '../../components/block';
import HeaderC from '../../components/headerC';
import images from '../../constants/images';
import LinearGradient from 'react-native-linear-gradient';
import {Container, Content} from 'native-base';
import styles from './styles';
import {colors} from '../../constants/theme';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import {storeData, getData} from '../../asyncStorage/AsynStorage';
import * as TypesAS from '../../asyncStorage/Types';

const {width, height} = Dimensions.get('window');

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputSearch: '',
      data: [],
      tokenMpoit1:''
    };
  }

  // static getDerivedStateFromProps(nextProps, currentState) {
  //   // Any time props.email changes, update state.
  //   if (nextProps.profile !== currentState.data) {
  //     this.setState({
  //       data: nextProps.profile,
  //     });
  //   }
  // }

  // componentWillMount() {
  //   this.setState({
  //     data: this.props.profile,
  //   });
  // }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (nextProps.profile !== nextState.data) {
  //     this.setState({
  //       data: nextProps.profile,
  //     });
  //   }
  // }

  componentDidMount(){
    this.getTokenMpoint()
  }

  getTokenMpoint(){
    getData(TypesAS.SET_TOKEN_MPOINT)
      .then(data =>this.setState({tokenMpoit1:data})).catch(e => console.log(e));
  }

  render() {
    const {navigation, profile, banner, tokenMpoit} = this.props;
    // console.log(this.state.data);
    console.log(this.state.tokenMpoit1);
    // console.log(this.props.profile.data.userInfo.name);
    const dataBannerHome = banner.filter(x => x.type == 1);
    return (
      <Container>
        <HeaderC
          // title="Trang chủ"
          // back
          official
          input={inputSearch => {
            this.setState({inputSearch});
          }}
        />
        <Content>
          <Block style={styles.content} flex={1}>
            <StatusBar
              backgroundColor={colors.PRIMARY}
              barStyle="light-content"
            />
            <LinearGradient
              colors={['#9bd3f0', '#3992c1', '#0571a9']}
              style={styles.lineGRa}>
              <ImageBackground source={images.ICONBG} style={styles.bgProfile}>
                <Block row>
                  <Block center middle style={{}} flex={1}>
                    <Block style={styles.contact} />
                    <Block center middle style={{marginTop: 10}}>
                      <Text style={styles.name}>{profile.name}</Text>
                      {profile.companyInfo ? (
                        <Text style={styles.name}>
                          {profile.companyInfo.name}
                        </Text>
                      ) : null}

                      {profile.point ? (
                        <Text
                          style={[styles.name, {marginTop: 10, fontSize: 11}]}>
                          mPoint: {profile.point}
                        </Text>
                      ) : null}
                    </Block>
                  </Block>
                  <Block center style={{}} flex={1}>
                    <Image
                      source={images.CARD}
                      resizeMode="contain"
                      style={styles.card}
                    />
                    <TouchableOpacity style={styles.touchntc}>
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 11,
                          fontWeight: '600',
                        }}>
                        Nhà tài trợ: Vàng
                      </Text>
                    </TouchableOpacity>
                  </Block>
                </Block>
              </ImageBackground>
            </LinearGradient>
            <Block style={styles.tab}>
              <Block center middle>
                <Text style={{fontWeight: '600'}}>
                  Câu lạc bộ Doanh Nhân Sài Gòn
                </Text>
                <Block
                  style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: 'gray',
                    marginBottom: 20,
                    marginTop: 5,
                  }}
                />
              </Block>
              <Block center middle row style={{marginBottom: 10}}>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={() => navigation.navigate('TradeScreen')}>
                  <Block center middle>
                    <Image
                      source={images.ICONLOA}
                      resizeMode="contain"
                      style={styles.iconreward}
                    />
                    <Text
                      style={{marginTop: 10, fontWeight: '600', fontSize: 12}}>
                      Giao thương
                    </Text>
                  </Block>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={() => navigation.navigate('EventScreen')}>
                  <Block center middle>
                    <Image
                      source={images.ICONHOITHAO}
                      resizeMode="contain"
                      style={styles.iconreward}
                    />
                    <Text
                      style={{marginTop: 10, fontWeight: '600', fontSize: 12}}>
                      Sự kiện
                    </Text>
                  </Block>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={() => navigation.navigate('FellowshipScreen')}>
                  <Block center middle style={{}}>
                    <Image
                      source={images.ICONNHOM}
                      resizeMode="contain"
                      style={styles.iconreward}
                    />
                    <Text
                      style={{marginTop: 10, fontWeight: '600', fontSize: 12}}>
                      Hội viên
                    </Text>
                  </Block>
                </TouchableOpacity>
              </Block>
            </Block>
            <Block style={styles.viewFlatHome}>
              <FlatList
                data={dataBannerHome}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id + ''}
                renderItem={({item, index, separators}) => (
                  <TouchableOpacity>
                    <View style={styles.viewFlat}>
                      <Image
                        source={{
                          uri: item.imageUrl,
                        }}
                        style={styles.imageFlat}
                      />
                    </View>
                  </TouchableOpacity>
                )}
              />
            </Block>
          </Block>
          <Block style={{height: 50}} />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.user.profile,
  banner: state.user.banner,
  tokenMpoit: state.user.tokenMpoit,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      // refreshtoken: userActions.refreshToken,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
