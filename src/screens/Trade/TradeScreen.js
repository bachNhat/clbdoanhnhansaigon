import React, {useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import Block from '../../components/block';
import {Container, Content, Icon} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import Carousel, {Pagination} from 'react-native-snap-carousel';
const {width, height} = Dimensions.get('window');
const SLIDER_1_FIRST_ITEM = 1;

const TradeScreen = () => {
  const [slider1ActiveSlide, setSlider1ActiveSlide] = useState(
    SLIDER_1_FIRST_ITEM,
  );
  const [newArr, setNewArr] = useState([]);
  const [carousel, setCarousel] = useState();

  const _renderItem = ({item, index}) => {
    console.log(item);
    return (
      <View style={styles.slide}>
        <Image
          source={{uri: item.url}}
          style={{width: '100%', aspectRatio: 30 / 15}}
          resizeMode="cover"
        />
      </View>
    );
  };

  const data = [
    {
      url:
        'https://cdnmedia.thethaovanhoa.vn/Upload/tyTrfgkgEUQwPYuvZ4Kn1g/files/2018/12/2012/gdh.jpg',
    },
    {
      url:
        'https://img.thuthuatphanmem.vn/uploads/2018/09/26/anh-noel-hai-huoc_051045803.jpg',
    },
    {
      url:
        'https://img.freepik.com/free-vector/santa-claus-friends-with-christmas-tree_43880-210.jpg?size=626&ext=jpg',
    },
    {
      url: 'https://i.ytimg.com/vi/EmlJskU2yGk/maxresdefault.jpg',
    },
  ];
  const dataTouch = [
    {
      id: 0,
      name: 'Ẩm thực',
      icon: 'pizza',
    },
    {
      id: 1,
      name: 'Mua sắm',
      icon: 'cart',
    },
    {
      id: 2,
      name: 'Sức khoẻ',
      icon: 'body',
    },
    {
      id: 3,
      name: 'Du lịch',
      icon: 'car',
    },
    {
      id: 4,
      name: 'Giáo dục',
      icon: 'book',
    },
    {
      id: 5,
      name: 'Làm đẹp',
      icon: 'color-wand',
    },
    {
      id: 6,
      name: 'Di chuyển',
      icon: 'train',
    },
    {
      id: 7,
      name: 'Khác',
      icon: 'more',
    },
  ];

  return (
    <Container>
      <HeaderC title="GIAO THƯƠNG" back />
      <Content style={{paddingTop: 5}}>
        <Carousel
          ref={c => setCarousel(c)}
          data={data}
          renderItem={_renderItem}
          sliderWidth={width}
          itemWidth={0.8 * width}
          autoplay={true}
          lockScrollWhileSnapping={true}
          // loopClonesPerSide={2}
          autoplayInterval={4000}
          useScrollView={true}
          loop={true}
          onSnapToItem={index => setSlider1ActiveSlide(index)}
        />
        <Block style={{}}>
          <Pagination
            dotsLength={data.length}
            activeDotIndex={slider1ActiveSlide}
            containerStyle={styles.paginationContainer}
            dotColor={colors.PRIMARY}
            dotStyle={styles.paginationDot}
            inactiveDotColor={'black'}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            carouselRef={carousel}
            tappableDots={!!carousel}
          />
        </Block>
        <Block style={styles.content}>
          <Text style={{fontSize: 16, fontWeight: '600'}}>
            BẠN CẦN KHUYẾN MÃI GÌ
          </Text>
          <Block
            row
            flex={1}
            style={{
              flexWrap: 'wrap',
              justifyContent: 'space-between',
            }}>
            {dataTouch.map((item, index) => {
              return (
                <Block
                  center
                  middle
                  style={{width: '24%', marginBottom: 5, marginTop: 10}}>
                  <TouchableOpacity>
                    <Block center middle>
                      <Block
                        center
                        middle
                        style={{
                          // backgroundColor: 'red',
                          width: 60,
                          height: 60,
                          borderRadius: 30,
                        }}>
                        <Icon
                          name={item.icon}
                          style={{color: colors.PRIMARY}}
                        />
                      </Block>
                      <Text>{item.name}</Text>
                    </Block>
                  </TouchableOpacity>
                </Block>
              );
            })}
            <Text style={{fontSize: 16, fontWeight: '600', marginTop: 20}}>
              CÓ THẺ BẠN NÊN XEM
            </Text>
            <Block center middle style={{marginTop: 5}}>
              <FlatList
                data={[1, 1, 1, 1, 1]}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id + ''}
                renderItem={({item, index, separators}) => (
                  <TouchableOpacity
                    onPress={() => console.log(item)}
                    style={{marginLeft: 10}}>
                    <View style={styles.viewFlat}>
                      <Image
                        source={require('../../assets/images/imageSK.jpg')}
                        style={styles.imageFlat}
                      />
                    </View>
                  </TouchableOpacity>
                )}
              />
            </Block>
            <Text style={{fontSize: 16, fontWeight: '600', marginTop: 20}}>
              KHUYẾN MÃI ẨM THỰC
            </Text>
            <Block center middle style={{marginTop: 5}}>
              <FlatList
                data={[1, 1, 1, 1, 1]}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id + ''}
                renderItem={({item, index, separators}) => (
                  <TouchableOpacity
                    onPress={() => console.log(item)}
                    style={{marginLeft: 10}}>
                    <View style={styles.viewFlat}>
                      <Image
                        source={require('../../assets/images/imageSK.jpg')}
                        style={styles.imageFlat}
                      />
                    </View>
                  </TouchableOpacity>
                )}
              />
            </Block>
          </Block>
        </Block>
      </Content>
    </Container>
  );
};

export default TradeScreen;
