import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    height,
    // backgroundColor: '#dddddd',
  },
  paginationContainer: {
    paddingVertical: 8,
  },
  paginationDot: {
    width: 10,
    height: 10,
    borderRadius: 8,
  },
  itemContent: {},
  imageFlat: {
    width: 250,
    height: 150,
    borderRadius: 8,
  },
  viewFlat: {
    width: '100%',
  },
});

export default styles;
