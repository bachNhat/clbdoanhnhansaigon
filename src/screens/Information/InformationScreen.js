import React, {Component, useEffect, useState} from 'react';
import {Text, View, FlatList, TouchableOpacity, Image} from 'react-native';
import Block from '../../components/block';
import {Container, Content} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import moment from 'moment';
const LIMIT = 10;
class InformationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      data: [],
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData = () => {
    // this.setState({skip:0})
    try {
      const {information} = this.props;
      information({skip: 0, limit: LIMIT, genres: 1});
    } catch (error) {
      console.log('err get data history', error);
    }
  };

  render() {
    const {informationRD, navigation} = this.props;
    console.log(this.props.informationRD);
    return (
      <Container>
        <HeaderC title="TIN TỨC" />
        <Content style={styles.content}>
          <FlatList
            data={informationRD}
            ListEmptyComponent={
              <Block center middle>
                <Text>Không có tin tức</Text>
              </Block>
            }
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.id + ''}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity
                style={styles.item}
                key={index}
                onPress={() =>
                  navigation.navigate('ContentInformationScreen', {item: item})
                }>
                <Block style={styles.viewFlat} row>
                  <Block flex={1} center>
                    <Image source={images.ICON_CENTER} style={styles.icon} />
                  </Block>
                  <Block flex={7}>
                    <Text style={{fontSize: 16, fontWeight: '600'}}>
                      {item.name}
                    </Text>
                    <Text style={{marginTop: 3}}>{item.lead}</Text>
                    <Text
                      style={{
                        fontSize: 12,
                        marginTop: 3,
                        color: colors.PRIMARY_DARK,
                      }}>
                      {moment(item.updatedAt).format('L')}
                    </Text>
                  </Block>
                </Block>
              </TouchableOpacity>
            )}
          />
          {/* </Block> */}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  informationRD: state.user.informationRD,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      information: userActions.information,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InformationScreen);
