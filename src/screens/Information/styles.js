import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    height,
    // backgroundColor: 'red',
  },
  item: {
    width: '100%',
    marginBottom: 5,
    paddingVertical: 10,
    borderBottomColor: 'gray',
    borderBottomWidth: 0.3,
  },
  icon: {
    width: 30,
    height: 30,
  },
});

export default styles;
