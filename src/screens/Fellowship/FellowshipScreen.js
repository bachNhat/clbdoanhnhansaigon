import React, {Component, useEffect, useState} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import Block from '../../components/block';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
const LIMIT = 10;

class FellowshipScreen extends Component {
  data = [
    {
      id: 0,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 1,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 2,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 3,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 4,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 5,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 6,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      id: 7,
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
  ];

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      data: [],
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData = () => {
    // this.setState({skip:0})
    try {
      const {company} = this.props;
      company({skip: 0, limit: LIMIT});
    } catch (error) {
      console.log('err get data fellowship', error);
    }
  };

  pushData() {
    // alert('hahah');
    const {company, allCompanyRD} = this.props;
    console.log(company.length);
    // if (!this.props.isStopLoadCompany) {
    try {
      company({skip: allCompanyRD.length, limit: LIMIT});
    } catch (error) {
      console.log('err get data history', error);
    }
    // }
  }

  render() {
    const {navigation, allCompanyRD} = this.props;
    return (
      <Container>
        <Header style={styles.header}>
          <Left style={{flex: 1, paddingLeft: 10}}>
            <TouchableOpacity onPress={() => navigation.pop()}>
              <Icon name="arrow-back" style={{color: 'white'}} />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 8}}>
            <Block row center>
              <TextInput
                style={styles.inputHeader}
                placeholder="Nhập tên công ty"
              />
              <Icon name="search" style={styles.iconSearch} />
            </Block>
          </Body>
          <Right style={styles.headerRightH}>
            <TouchableOpacity>
              <Text style={styles.textHuy}>Huỷ</Text>
            </TouchableOpacity>
          </Right>
        </Header>
        <Content style={styles.content} contentContainerStyle={{flex: 1}}>
          <FlatList
            data={allCompanyRD}
            ListEmptyComponent={
              <Block center middle>
                <Text>Không có tin tức</Text>
              </Block>
            }
            // onEndThreshold={0}
            // refreshing={false}
            // onRefresh={this.getData}
            onEndReached={() => this.pushData()}
            onEndReachedThreshold={0.1}
            keyExtractor={item => item.id + ''}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity key={index}>
                <Block row style={styles.item}>
                  <Block style={{paddingLeft: 8}} flex={1.5}>
                    <Block style={{borderRadius: 8}}>
                      <Image
                        resizeMode="cover"
                        style={styles.imglogo}
                        source={{
                          uri:
                            item.image ||
                            'https://seekcomau.corewebdna.net.au/web_images/blogs/214/1564/What%20can%20a%20company%20offer%20me_940x485.jpg',
                        }}
                      />
                    </Block>
                  </Block>
                  <Block style={{paddingHorizontal: 8}} flex={2}>
                    <Text style={{fontSize: 15, fontWeight: '600'}}>
                      {item.name}
                    </Text>
                    <Text style={{marginTop: 3}}>{item.description}</Text>
                  </Block>
                </Block>
              </TouchableOpacity>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  allCompanyRD: state.user.allCompanyRD,
  // isRefreshing: state.user.isRefreshing,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      company: userActions.company,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FellowshipScreen);
