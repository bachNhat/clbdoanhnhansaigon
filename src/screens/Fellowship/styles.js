import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.PRIMARY,
    height: 80,
  },
  inputHeader: {
    width: '90%',
    height: 40,
    backgroundColor: 'white',
    borderRadius: 20,
    paddingLeft: 40,
  },
  iconSearch: {
    position: 'absolute',
    color: 'gray',
    left: 10,
  },
  item: {
    width: '100%',
    marginBottom: 5,
    paddingVertical: 10,
    backgroundColor: '#eeeeee',
    borderRadius: 8,
  },
  content: {
    padding: 5,
    width,
    height,
  },
  imglogo: {
    width: '100%',
    aspectRatio: 395 / 236,
    borderRadius: 8,
  },
  textHuy: {
    color: 'white',
    fontSize: 15,
    fontWeight: '600',
  },
  headerRightH: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
