import React, {Component, useEffect, useState} from 'react';
import {Text, View, FlatList, TouchableOpacity, Image} from 'react-native';
import Block from '../../components/block';
import {Container, Content} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';
import HTML from 'react-native-render-html';
import moment from 'moment';
const LIMIT = 10;
class EventScreen extends Component {
  data = [
    {
      id: 1,
      tieuDe: 'Day la tieu de tin tuc',
      noiDung:
        'day laf noi dung tin tuc dai vai ca cut luon nhung ma thuc su no khong dai lam',
      thoiGian: '20:05 - 14/11/2019',
    },
    {
      id: 2,
      tieuDe: 'Day la tieu de tin tuc',
      noiDung:
        'day laf noi dung tin tuc dai vai ca cut luon nhung ma thuc su no khong dai lam',
      thoiGian: '20:05 - 14/11/2019',
    },
    {
      id: 3,
      tieuDe: 'Day la tieu de tin tuc',
      noiDung:
        'day laf noi dung tin tuc dai vai ca cut luon nhung ma thuc su no khong dai lam',
      thoiGian: '20:05 - 14/11/2019',
    },
    {
      id: 4,
      tieuDe: 'Day la tieu de tin tuc',
      noiDung:
        'day laf noi dung tin tuc dai vai ca cut luon nhung ma thuc su no khong dai lam',
      thoiGian: '20:05 - 14/11/2019',
    },
  ];

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      data: [],
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData = () => {
    // this.setState({skip:0})
    try {
      const {information} = this.props;
      information({skip: 0, limit: LIMIT, genres: 2});
    } catch (error) {
      console.log('err get data history', error);
    }
  };

  render() {
    const {eventRD, navigation} = this.props;
    console.log(this.props.eventRD);
    return (
      <Container>
        <HeaderC title="SỰ KIỆN" back />
        <Content style={styles.content} contentContainerStyle={{flex: 1}}>
          <FlatList
            data={eventRD}
            // data={this.data}
            ListEmptyComponent={
              <Block center middle>
                <Text>Không có tin tức</Text>
              </Block>
            }
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.id + ''}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity
                style={styles.item}
                key={index}
                onPress={() =>
                  navigation.navigate('ContentInformationScreen', {item: item})
                }>
                <Block style={styles.viewFlat}>
                  <Block flex={1} center>
                    <Image
                      source={{uri: item.image} || images.LOGO_DNSG}
                      style={styles.icon}
                      resizeMode="contain"
                    />
                  </Block>
                  <Block flex={7}>
                    <Text
                      style={{fontSize: 16, fontWeight: '700', marginTop: 10}}>
                      {item.name}
                    </Text>
                    <Text style={{marginTop: 3}}>
                      {moment(item.thoiGian).format('L')}: {item.lead}
                    </Text>
                  </Block>
                </Block>
              </TouchableOpacity>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  eventRD: state.user.eventRD,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      information: userActions.information,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventScreen);
