import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  imageHeader: {
    width: '100%',
    height: (19.12 * width) / 32.53,
  },
  content: {},
  textTop: {
    color: colors.WORD,
    fontSize: 18,
    fontWeight: '700',
    marginTop: 20,
  },
  touch: {
    paddingVertical: 10,
    paddingHorizontal: 60,
    backgroundColor: colors.PRIMARY,
    borderRadius: 12,
    marginTop: 30,
  },
  textCard: {
    marginTop: 30,
    fontSize: 18,
  },
  card: {
    width: '90%',
    height: (14 * width) / 22,
  },
});

export default styles;
