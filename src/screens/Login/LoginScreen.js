import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Block from '../../components/block';
import images from '../../constants/images';
import styles from './styles';
import {colors} from '../../constants/theme';
import SplashScreen from 'react-native-splash-screen';
import storeT from '../../utils/store';
import {storeData, getData} from '../../asyncStorage/AsynStorage';
import * as TypesAS from '../../asyncStorage/Types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {userActions} from '../../state/user';

class LoginScreen extends Component {
  refreshToken() {
    const {refreshtoken} = this.props;
    getData(TypesAS.SET_TOKEN_LOGIN)
      .then(data =>
        // refreshtoken({
        //   token: JSON.parse(data),
        //   success: this.props.navigation,
        // }),
        refreshtoken({
          token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImNyZWF0ZWRBdCI6MTU3NTk5MjU0MDQzOCwidXBkYXRlZEF0IjoxNTc1OTkyNTg4NjY5LCJpZCI6ODcxLCJuYW1lIjoiZGVtbyIsInBob25lIjoiODQxMjIzMzc1NDY2IiwiZW1haWwiOiJkZW1vQDEyMy5jb20iLCJpZGVudGlmaWNhdGlvbiI6IjAxMjM0NTY3ODk4MjciLCJiaXJ0aCI6MCwiZ2VuZGVyIjoyLCJhZGRyZXNzIjoiaMOgIG7hu5lpIiwicG9pbnQiOjEyMzUzMTIzLCJ1c2VybmFtZSI6Im1lZGlhb25lZGVtbyIsImF2YXRhciI6bnVsbCwicGhvbmVBdXRoIjpmYWxzZSwiY2hhbmdlUGFzc3dvcmRBdCI6MTU3NTk5MjU0MDQzOCwibm90aWZpY2F0aW9uIjowLCJjb21wYW55IjowLCJ0aW1lQWNjZXNzIjowLCJhY3RpdmUiOjEsInJvbGUiOjN9LCJjcmVhdGVkQXQiOiIyMDE5LTEyLTExVDAxOjQ3OjMzLjQ0MFoiLCJleHBpcmVkQXQiOiIyMDIwLTAyLTA5VDAxOjQ3OjMzLjQ0MFoiLCJpYXQiOjE1NzYwMjg4NTN9.BOfbGEokoVQhAU02Qzq8jqNJiOyNXNYD-YuJe7pALu8',
          success: this.props.navigation,
        }),
      )
      .catch(e => console.log(e));
  }
  componentDidMount() {
    SplashScreen.hide();
    // this.refreshToken();
  }
  render() {
    return (
      <ScrollView>
        <SafeAreaView style={{backgroundColor: colors.PRIMARY}} />
        <Image
          source={images.HEADER_LOGIN}
          resizeMode="contain"
          style={styles.imageHeader}
        />
        <Block center middle styles={styles.content}>
          <Text style={styles.textTop}>VÍ ĐỰNG THẺ HỘI VIÊN TRÊN MOBILE</Text>
          <Text style={{marginTop: 5}}>
            Quét mã QR sau để đăng nhập mobile app
          </Text>
          {/* <Text style={{marginTop: 5}}>{storeT.getToken()}</Text> */}
          <TouchableOpacity
            style={styles.touch}
            onPress={() => {
              this.props.navigation.navigate('CameraScanScreen', {
                from: 'loginScreen',
              });
            }}>
            <Text style={{color: 'white', fontWeight: '500', fontSize: 18}}>
              Quét mã
            </Text>
          </TouchableOpacity>
          <Text style={styles.textCard}>BẠN ĐANG DÙNG THẺ</Text>
          <Image
            source={images.CARD_DEMO}
            resizeMode="contain"
            style={styles.card}
          />
        </Block>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.user.profile,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      refreshtoken: userActions.refreshToken,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
