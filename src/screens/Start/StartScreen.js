import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import Block from '../../components/block';
import images from '../../constants/images';
import styles from './styles';

export default class StartScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Login');
    }, 3000);
  }
  render() {
    return (
      <Block style={{backgroundColor: 'white'}} flex={1} center middle>
        <Image
          source={images.LOGO_DNSG}
          resizeMode="contain"
          style={styles.imageLOGO}
        />
      </Block>
    );
  }
}
