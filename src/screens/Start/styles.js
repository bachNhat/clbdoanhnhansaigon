import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  imageLOGO: {
    width: 0.9 * width,
    height: 0.5 * height,
    marginBottom: 100,
  },
});

export default styles;
