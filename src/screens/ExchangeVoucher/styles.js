import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    height,
    backgroundColor: '#dddddd',
  },
  picker: {
    width: '100%',
    backgroundColor: 'white',
    paddingLeft: 10,
  },
  touch: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    width: '100%',
    paddingVertical: 5,
  },
  itemContent: {
    width: '100%',
    // height:100,
    backgroundColor: 'white',
    borderRadius: 8,
    marginBottom: 5,
  },
  imglogo: {
    width: 50,
    height: 50,
    borderRadius: 8,
  },
  uriImage: {
    width: '100%',
    height: 200,
  },
  newTag: {
    width: 130,
    height: 50,
    position: 'absolute',
    right: 0,
    top: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
