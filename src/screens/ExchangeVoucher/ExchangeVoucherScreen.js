import React, {useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput,
} from 'react-native';
import Block from '../../components/block';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Picker,
  Form,
} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';

const ExchangeVoucherScreen = ({navigation} = this.props) => {
  const [sort, setSort] = useState('key0');
  const [idItem, setIdItem] = useState(0);

  const data = [
    {
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
    {
      url:
        'https://cdn.cellphones.com.vn/media/wysiwyg/accessories/earphone/tai-nghe-bluetooth-jbl-t450bt-1.jpg',
      title: 'LOTTE Members Card',
      content: 'Loving your life',
    },
  ];

  const dataTouch = [
    {
      id: 0,
      name: 'Tất cả',
    },
    {
      id: 1,
      name: 'Ẩm thực',
    },
    {
      id: 2,
      name: 'Mua sắm',
    },
    {
      id: 3,
      name: 'Sức khoẻ',
    },
    {
      id: 4,
      name: 'Du lịch',
    },
    {
      id: 5,
      name: 'Giáo dục',
    },
    {
      id: 6,
      name: 'Làm đẹp',
    },
    {
      id: 7,
      name: 'Khác',
    },
  ];

  const dataItem = [
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
    {
      logo: 'https://vincom.com.vn/sites/default/files/2016-09/VinMart_14.jpg',
      storeName: 'VINMART',
      summary: 'Lẩu nấm thiên nhiên',
      uriImage:
        'https://imagevietnam.vnanet.vn/Upload/2016/7/19/190720160952163-COVER-13_resize.jpg',
      content:
        'Mã quà tặng Mobile Gift 500.000 VNĐ sử dụng tại nhà hàng lẩu nấm ASHIMA',
      timeApply: '20/05/2020',
      mete: 'Toàn quốc',
    },
  ];

  const onValueChange = (value: string) => {
    setSort(value);
  };

  const renderHeaderFlatlist = () => {
    return (
      <Block
        row
        flex={1}
        style={{
          flexWrap: 'wrap',
          justifyContent: 'space-between',
        }}>
        {dataTouch.map((item, index) => {
          return (
            <Block center middle style={{width: '24%', marginBottom: 5}}>
              <TouchableOpacity
                style={[
                  styles.touch,
                  {
                    backgroundColor:
                      idItem == item.id ? colors.PRIMARY : 'white',
                  },
                ]}
                onPress={() => setIdItem(item.id)}>
                <Text
                  style={{color: idItem == item.id ? 'white' : colors.PRIMARY}}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            </Block>
          );
        })}
      </Block>
    );
  };

  return (
    <Container>
      <Content>
        <Block style={styles.picker} row center>
          <Icon name="filter" type="AntDesign" style={{color: 'gray'}} />
          <Text style={{marginLeft: 5}}>Sắp xếp theo</Text>
          <Form>
            <Picker
              mode="dropdown"
              iosHeader="Sắp xếp theo"
              iosIcon={
                <Icon
                  name="arrow-down"
                  style={{color: colors.PRIMARY, marginLeft: -10}}
                />
              }
              style={{width: undefined}}
              selectedValue={sort}
              onValueChange={onValueChange}
              textStyle={{
                color: colors.PRIMARY,
                fontSize: 14,
                fontWeight: '600',
              }}
              placeholderStyle={{color: colors.PRIMARY}}
              headerBackButtonTextStyle={{color: colors.PRIMARY}}
              headerTitleStyle={{color: colors.PRIMARY}}
              placeholderIconColor={{color: colors.PRIMARY}}>
              <Picker.Item label="Mới nhất" value="key0" />
              <Picker.Item label="Cũ nhất" value="key1" />
              <Picker.Item label="Ưa thích" value="key2" />
            </Picker>
          </Form>
        </Block>
        <Block flex={1} style={styles.content}>
          <FlatList
            data={dataItem}
            keyExtractor={item => item.id + ''}
            ListHeaderComponent={renderHeaderFlatlist}
            renderItem={({item, index, separators}) => (
              <TouchableOpacity style={styles.itemContent}>
                <Block row style={{padding: 5}}>
                  <Image
                    resizeMode="contain"
                    style={styles.imglogo}
                    source={{uri: item.logo}}
                  />
                  <Block style={{marginLeft: 10}}>
                    <Text style={{fontWeight: '600', fontSize: 16}}>
                      {item.storeName}
                    </Text>
                    <Text style={{color: 'gray'}}>{item.summary}</Text>
                  </Block>
                </Block>
                <Block>
                  <Image
                    resizeMode="cover"
                    style={styles.uriImage}
                    source={{uri: item.uriImage}}
                  />
                  <ImageBackground
                    // resizeMode="contain"
                    style={styles.newTag}
                    source={images.NEWTAG}>
                    <Text
                      style={{color: 'white', fontWeight: '700', fontSize: 18}}>
                      500.000
                    </Text>
                    <Text style={{color: 'white', fontSize: 12, marginTop: 2}}>
                      điểm mPoint
                    </Text>
                  </ImageBackground>
                  <Block
                    center
                    middle
                    style={{backgroundColor: '#b61c1c', paddingVertical: 5}}>
                    <Text style={{color: 'white'}}>Đổi E-Voucher</Text>
                  </Block>
                </Block>
                <Block
                  style={{
                    padding: 5,
                    borderBottomColor: 'gray',
                    borderBottomWidth: 0.3,
                  }}>
                  <Text style={{color: 'gray'}}>{item.content}</Text>
                </Block>
                <Block
                  style={{
                    padding: 5,
                    paddingRight: 50,
                    justifyContent: 'space-between',
                  }}
                  row>
                  <Block row center style={{}}>
                    <Icon
                      name="calendar"
                      style={{fontSize: 20, marginRight: 5, color: 'gray'}}
                    />
                    <Text style={{color: 'gray'}}>
                      Áp dụng tới {item.timeApply}
                    </Text>
                  </Block>
                  <Block row center>
                    <Icon
                      name="pin"
                      style={{fontSize: 20, marginRight: 5, color: 'gray'}}
                    />
                    <Text style={{color: 'gray'}}>{item.mete}</Text>
                  </Block>
                </Block>
              </TouchableOpacity>
            )}
          />
        </Block>
      </Content>
    </Container>
  );
};

export default ExchangeVoucherScreen;
