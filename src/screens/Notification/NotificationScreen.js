import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import Block from '../../components/block';
import HeaderC from '../../components/headerC';
import images from '../../constants/images';
import LinearGradient from 'react-native-linear-gradient';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Item,
} from 'native-base';
import styles from './styles';

const {width, height} = Dimensions.get('window');

function Img({avt}) {
  console.log(avt);
  return (
    <Block>
      <Image style={{width: 50, height: 50}} source={avt.image}></Image>
    </Block>
  );
}
export default class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputSearch: '',
      data: [
        {
          image:
            'https://i.pinimg.com/236x/28/0b/be/280bbe85c8d041715e6d0372b108ad2f.jpg',
          name: 'Katherine',
          message: 'hello',
          time: '5:10 PM',
        },
        {
          image: `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAIIAggMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIEBQMHBv/EADcQAAIBAgQCBwYFBAMAAAAAAAECAAMRBBIhMQVBE1FhcYGRoQYUIjKxwTNCctHwI1Jz4RWSsv/EABgBAAMBAQAAAAAAAAAAAAAAAAABAgME/8QAHhEBAQACAgMBAQAAAAAAAAAAAAECEQMxEiFBEzL/2gAMAwEAAhEDEQA/APXITpTpl2sDaRZcpsZx69bde5vSJijgYjIR3ihAHeAijuIAREgXJNgNyYqjLTR6lRgqKCzMdgBznnnHuN4jitdqSO1PCAnKg0uBzbt/nKVjjsrdPr6/tLwPDsVq8VwmYbhagYjyvLOC4tw/iBtgcZQrsNSqOMw8N55VUVGxFMZFzEHKbd/7eslmt0b5iDm+F+asP54S/CJ8q9g5RWnzfsnx5scnueLe+JQfCx3cD7j1HjPpJnZpUuwDHFGIjEIQgEw9triRJvvFCMtCBhEYjAhGBFAGI/hy7HNIxwDE9rcemD4Z0bJnOIJQLe3L97ec8/SpQfHMqI6KDlADg7L3fqn1vt0lRqmCIW9Jb3PUcyfzwnx3u7ir0itbOdOw3v8AbyM2w6R9OvTQ4zDlSwZQSpO3cfAx1EBw9YWJuSQL89/rIVw3SU6uqZQykEdtxbrt9pLpEs6sMrjUjs/npKGljh3EK2CxArYdER3VH+W97crnbtnqdJ1q00qU9UdQynsM8mqladdddECL4E/ss9O4Kz/8VhUrLlrU6SpUX+0gWmeZxdMBAxgTNQhCEAITpVp9GbA3nOVZopd9CLnHFJM4ucIQAtHAQgGV7TUPeOD1kClnJVUUbklgLeM87cVUD0WX4kfMp5gggkEefnPV3QMVLC+U3HfPkvanhIo9PxGmLCpUHSAcgQBfzB8xNMctekvlC5F0KkC17EadnjtOtl6JSVRyPl0sV527tJ0ejUqMwoUywojpavPQEad+vlFkFSq1NFzHKGUDnvtL2Z8D4ceJ8apqtMiiWz1OQyqf928Z6iJn8G4anDcElIKOlIvVYc25+HLwmheZZXYgEDFeOSYhCEACx67xXkQZ0SmWOxEZeojeE6jD3a2ssJh0UbXlTC1N5JFGOXjhkJvaRbDIAbXj/Op/WKYMLzslBs2o0hUNKi/xsO61z5RTC1VzkcbyrxOgmL4fiMPUYqtVCt13udrTvisQpQ9ApB3zMPoJnVqrKoqVDUyjUOPjXvI38pU4sk3liWFwdDCUxToUwi637b73PO5lMcGwVPiNHHopR0Y3sdLNvp36yyleqQCrUagPO7IfI3+sb1KiqWqVMPTQbln2mnimZVqQmZTepRS3vBe3yqtMW7teXiJao13NNemp5Xt8QT4h+8yvHlGk5JVmO85LURxdTe2/ZJgyFpQivCAWKOGI1aWlQSQEdp0TGRyXK0sojhCUkQhCAc67rSpNUbZQTMS7MSXOZm1Y9ZmlxQkYcAc218NfraZ3ZKhuZormzLmRueU2v4bSDXoEm16J1Yf2f6+k7xHrG8YVThhlLYcrY62J0EguGyMCwNesNR+VE7ezxuZcREHyADu0kgANhpAOPu6tlNY52HVoo7hOoAGwtHzk8ogEL5TnG9rHunacxa5HVOlGmzU0trpY+Gkx5cfrbjy+CE6+7mEx1WnlGnCERM6XIcIo4AQhCM1Him1G+2Y/SZWHe4CtqyqVPeptNDiZvWoqflCs3joPufOYmFcvxSoyX6JlYENycMB9ow0I+UIxsYyRtz8+2SBFttYoAQCWlpJCuYZtr6yMcAr4qutOq3RA5Wr06YHPW1/S81OHlTRtzDEEdWt/uJikA4ts5sqVM/YbLYfX0mnw6pau9M/mXOPCwP285NNpZRCKEQT3haOEoCEIRaAhCEYY+OqH3moSCLCwB5gc/MmZeFQU8WSD+JdrdttfpNrjtFK2FVW36VSD1TGShUo16JLlkD21N9wR3yfL2cm4v3kHbKQ19DoZK15FhnLUzsVvfq/mkokrx3lTDV7t0dTRh69frLdoAXheFooBSZ81Wpbk1j4TQwFUGvRAX4mBX9z3aCZNSjUevWC1KoBfZLC3pf1m17PUUpYeqoplXFQgu2pbQHfnvJmUvpVx1NtUAWijsIR6ScIQjAhCEAIQhAM7jrtT4e7ILurKVX+6xuR5AzPq2/p9WcTQ4rc1aC30s58dB9zM+sy0hTdzZQ4B8fhHqRM72c6dpBl/qqewi387pOO00Jm46ky1Q6fm1X9Q5eI+ktYOp0tINz2InapTDoVPOVELUahDDUb9o64BciIktxveRPLvtAK+H1r4r/ILf9FmrwWotbAJXS+WoWIv1XsPQCZODbpFeqPztf009LTY4SLYXKAAFdrD1+8zx7VV2EITRIhCEAIQhACEIQChxT5qHe30mRxgA8NqggEEqCDzFxHCZ5f0cWx8o7oQhNCS5SviALIba6i8IRUOlP8ADXuE54n5V7z9IQhOg5cN/Af/ACv/AOjNnhn4L/rP0EITPDtWS5CEJql//9k=`,
          name: 'Rose',
          message: 'how old are you',
          time: '4:30 PM',
        },
        {
          image: `https://diendanlequydon.com/downloads/image_prv/102/101432.jpg`,
          name: 'John',
          message: 'em an com chua?',
          time: '3:10 PM',
        },
        {
          image: `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAIIAggMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQIDBQYEB//EAEQQAAEDAgMEBwQHBQYHAAAAAAEAAgMEEQUSIRMxQVEGIjJhcZGxFHOBwTRCUmKTodEjJDNU8VOCksLh8BYlQ0RyorL/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMABAX/xAAeEQEBAAIDAQEBAQAAAAAAAAAAAQIREiExAyIyE//aAAwDAQACEQMRAD8A9aQhC5ndUgPegnmbqO6LrF0UnVJvQs90i6RigLqWis6qHaeRdsX6u9PyRk2Fy0t67EqLDmtdW1McWbstOrj4AalVf/F2FbQNHtBb9vZaet/yWFlkkmldNNI6SR5u57jclI1WnziN+lehHpXhDHACaZ33mwut6Lrocew6vkDIKppldujkBY53gDv+C82Ye8hNcAW2IFluEaZ16y7vTVh8C6TzUj2U+IyGWlNgJHavi8TxH5rbgggEEEEXBHFSyx0thlKVCEJThCENDsxJII4C25ELWarB++T+8d6oTqwfvc/vHeqFNwdtGhCE70AhCFmV3SDEhheHulYf27zkiH3ufwGvkvNyS4lziSSSSSd5Wg6ZVftGLNgabx0zcv8AeOp+Q+CoMt2j7xV8JqObK7pLdVKeqLpzQD4k/O3yUksdmX7x6hHYaRHQhJvKndGQ5nj8lCW2dHfjofT1IW22jSFrOhmLE/8ALKh24E07ifNvzHxWXe3LqnQyvpZ454j143h7fEFa9tOq9TSgEpkMjJoWSx9iRoc3wIupWqDp30TKlA0Tw0bPNmF+SaDosXe2arB+9z+8d6oS1n0yf3jvVKpONfoQhUd4QXNYwvfo1vWJ5AIVf0hk2WB1ruJjyf4iB80Z6XK9PPppnVNTLO/fK8vPdc3UMWr6Vp+s0k/AJJ35IJ3fZjPna6nczZ1lGOBDmjyCtbpGTZ1FDtXtaN+xD/K5K6pItpQueBc5bj4J2ASGLG8OvBI9hbs32bYG4I4960NRg8lJI9oikMD9W3sS3xsp2nkkumfbT7ZlMR9dwB8SCPVcNZEdjJk0c05m/wB7d/7K3io6mCnyCOVwzF0b2sJyuYQbbuIsR8eSdjFE+CVz3U8jYHC9wW9h3x4G6EprIpJS19A6obudGXDyTHtsy6iMhZSVNI5pDmyaC3A6+S66lloz32VJUrG66LTbbAqYE6x3j+AOn5EK3aQs10Kf+51MX2Zc3mB+i0anl6rh/J+iHOCZdBSjpnKwj2yf3jvVCbWfTJ/eO9UJHHppUIQndwVL0vfkwRw+3KxvqfkrpZ/psSMKgHOpb/8AL02PpM/GArXWgnHO4/JavB6CGsxClbUNLmsdcAG3BZ2kwyfGZKqGnfGwxPJc6UkAAgABafo44vqqc5iwlpsRvBylNS4zpuLUtHGOqyNo3WCmhljnZmjcHN3LFMwLpJUvO06Tva0n/pg3PlZa/DKeWkoIaeoqX1MrG2dM/e880EbNJwxo3NA+CgdVUpl9ne5uYnLlLd/cugEO3EFZbpDgONVle6ow7pFLQwOAGxAJAPPeiEduN4BhU1NPUPo42y7O2ePqmwvbd4lef1o/ZN/8mj8wtZSUeI4dQ1xxPGX4g0wmwc2wbvusrVRvla3KNIxtX9zWi5WlVk/LQdB/+/8AGP8AzLUrM9Bmn2WskI7UjG38AT81pkuXqnz8CEISnrNVn0yf3jvVCKz6ZP7x3qhI4mlQhCd2hUXTNl8E2n9lOx3wN2/5lerjxmlFbhNVTWJL2aAbyRqPRNj6XLx5zgWKvwfHqlwaJI5mNL4ybX04HnorTBMVpW1zZ5HCCBtQW/tDo0Ou0AndvNlXYv0crW1pNO0yythMkOUaTsB1t96xvbx7lVVBfQbGSRt6fEIGuDraZuXnp43T2JSvb4omtsQB4orKk00bXiCWUXs7Z26vebkaeC8hwTHq3Cg1lDVObDuED+tGPAHd8LL0XBcbra+jfU1OH5Y2C4ew9vS+jT+qBMsb6sqHEWVcxjip5WsDcxk6uS/K4O9dj2hw1F/guMVu0JbSQmV4YXAE5R3a671h8Z6Z4qyWSnbHHRPY4tcA3M4Eabzpb4INq76aHphOykwacaB0gDGjmSbfqvP6mvcyono42i8sTY3OvqAXZiPiG+Sq6nE56utbPVyS1Dmne9xJcRrYch/vgrjoVg1RjFfNXVAIgYM2Y6BzyNAPAWP9UZNdqW9abjorTmmwSHMLPlJkPxOn5WVukY0MYGtFmtFgOQSpLVZNQIQhAazVZ9Mn9471Qis+mT+8d6oSOJpUIQndoQhCzdEyts0ZR1Tdvce7zK5K7DKKugENTTMcwP2jbCxa69yQfHfz43XYkJDQXHQDUlNslxjNt6K4RHiFGxsT3hkpeWvdcAG1m+Fxf4kcra72eVrnCFzWRvIJsNRoBpw3AKko3Z6+Mutne/MQtQE2F3tP748NRWxwOpmufFs4mlxaQ86DXQ/6eiquleA0WKYZsnuc2ogGZkjCM+u/eOKvMUpjVUckbQC82y35qqmpX08dRLI0Xfka05iSLb95Pcmy6Tw7yilw/odgTKWEsjlljc0F20f/ABBvs6w3cwLX43WhghipoRDTxtjjF7NaLDXeubDTZkkR+qcw14H/AFv5rsUt7jp4SUIQhAwQhCzVmqz6ZP7x3qhFZ9Mn9471QkcTQPnjjaXyPaxg1LnGwHxUbK+jf2KuB3hKCqierBgkDr2LbWAuq3bSOHUhd3F7so+Z/JO6eTV+10v8zD+IEGspRvqYfxAsplmcetNkHKMfM/6JRTx73AvPN5zW89y225VqWV1G/VlXA7wlafmmVVVTup3tZPE4usLNeCTcrO9yiZUNLmmJpkDXAuc3dbx/RbYzLdizOZk8U1zdj75Se13f75K3GJS5ZDEXERNzvzi2nId+/wAlUZw9+ZlnZmHIQd5UzMtdTVNDSTyUj6mHqzsb12C1rgHj+qTlYt95L2WPpSaiTZQwzbUtcQHZeAJ4X5WU2MVjZoBC2fM5zmntaWvz4XTaiOWnnZXNe6CGBr2SU+VtqguDQ1xPAjLp42XFFFBC5tPCAQIg2RrTcAjT9fJH/S2dp/LGXJ20cwinDqh0cYIIJzaeZtyXY2to3E5auA20NpAs7VvErY4XHMB1n2PDcPNRtGVoa3Ro3AbgtL0P2us+mo9rpf5mH8QIbV0znFraiEuAvYSC6yksjmWayzpHdkH1PcliZs22uXEm7ieJTbT5VrdtF/ax/wCIJc7DucD4G6yll1YZVWikvp1yN/LRbY8qbWG9XP7x3qkXBVVI9pl39s+qRT25O04J4pwTXOawFz3BrRqSToFze3xP/gywgfbleAPLefyTuh1uIa0ucQAN5O4KHamT+AzN992jf1KiY+kdI0y1MUpvvdILA2O4blKyqpy9rX1EOnDONdFtMBAHfx3bT7trN8v1upm247lz+1U4YHCqhIva+cJr66lYwu20LrcGzXWZcCOFtHA0wsM813l+QXaP6WCQUcABGyGpv335356rgwnEIa4Ms5hkiiynru01/orNpNtL8Lm6XL12/KTigY2TOac5iwb3louW8Bmte/x4d6fJA2OAiPM0AatB7QHDy0S9YSPsD3bz81JbWwuRYX6x05oH44zyIMSjijnZsLBjogQBx1Oq4pHiNuY87ADeTyC4nYoySpgabMZFDs+tKBuJ1+Seypge/aSSRtIHUaZhoOZ709jgy9dELCLvk/iO323AcgpFD7ZTfzUH4gR7XS/zMH4gQ0Cbgoqdxax1vtv9Sk9rpf5mD8QKOnkjkjcY3tdaR3ZN+JWZw1Dzt5Nfrn1SJlQDt5Nfrn1QpuQ2rmlNPKDK8jKfrFUu0fftu3c0ITQCskfftu3nj3JsUj79t2/n3IQiBWySZW9d3a59y6GSPsOu7eeKEJp4yzwSaVrpcsrx1Ruce9Wj55nNbeWQ+LjyQhSy9dPyv5O2j9o/ru38/BIZpWv6sjxcHc4oQgpLWRmkk2jznd2efemukeMtnu7A4oQqOSgyPt23eaNo/wC27zQhGgBI/wC27zVlhksghfaR3a59yEIUIgnml28n7R/aP1jzQhCg5bX/2Q==`,
          name: 'Mark',
          message: 'goodbye',
          time: '2:50 PM',
        },
      ],
    };
  }

  render() {
    const {navigation} = this.props;
    return (
      <Container>
        <HeaderC title="THÔNG BÁO" back />
        <Content>
          <FlatList
            data={this.state.data}
            renderItem={({item}) => (
              <Block style={styles.eachBlock}>
                <Image source={{uri: item.image}} style={styles.avata}></Image>
                <Block style={styles.blockNameMessage}>
                  <Text style={styles.name}>{item.name}</Text>
                  <Text style={styles.message} numberOfLines={3}>
                    {item.message}{' '}
                  </Text>
                </Block>
                <Text style={styles.time}>{item.time}</Text>
              </Block>
            )}></FlatList>
        </Content>
      </Container>
    );
  }
}
