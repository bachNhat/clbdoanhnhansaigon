import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
import {black, red} from 'ansi-colors';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  eachBlock: {
    marginBottom: 10,
    flexDirection: 'row',
    borderRadius: 5,
    margin: 5,
    paddingHorizontal: 20,
    paddingVertical: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  avata: {
    width: 70,
    height: 70,
    borderRadius: 50,
  },
  blockNameMessage: {
    flex: 1,
    marginLeft: 20,
  },
  name: {
    color: 'black',
    fontWeight: '700',
  },
  message: {
    color: 'grey',
  },
  time: {
    color: 'grey',
  },
});

export default styles;
