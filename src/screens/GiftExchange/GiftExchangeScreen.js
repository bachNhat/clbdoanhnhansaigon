/* eslint-disable react/jsx-no-duplicate-props */
import React, {Component} from 'react';
import {Text, Platform, Dimensions} from 'react-native';
import Block from '../../components/block';
import {TabBar, TabView, SceneMap} from 'react-native-tab-view';
import ExchangeVoucherScreen from '../ExchangeVoucher/ExchangeVoucherScreen';
import ExchangeProductScreen from '../ExchangeProduct/ExchangeProductScreen';
import {colors} from '../../constants/theme';
import HeaderC from '../../components/headerC';
import {Container, Content} from 'native-base';

const {width, height} = Dimensions.get('screen');

const DoiVoucher = () => <ExchangeVoucherScreen />;
const DoiSanPham = () => <ExchangeProductScreen />;

export default class GiftExchangeScreen extends Component {
  state = {
    inputSearch: '',
    index: 0,
    routes: [
      {key: 'doiVoucher', title: 'ĐỔI VOUCHER'},
      {key: 'doiSanPham', title: 'ĐỔI SẢN PHẨM'},
    ],
  };

  _handleIndexChange = index => this.setState({index});

  _renderHeader = props => (
    <TabBar
      pressOpacity={0.1}
      pressColor="red"
      {...props}
      style={{backgroundColor: '#fff'}}
      tabStyle={{flexDirection: 'column'}}
      indicatorStyle={{backgroundColor: colors.PRIMARY}}
      renderLabel={payload => {
        return (
          <Text
            style={{
              color: payload.focused ? colors.PRIMARY : 'black',
              fontWeight: '500',
              fontSize: 20,
              fontSize: Platform.OS == 'ios' ? 12 : 13,
              textAlign: 'center',
            }}>
            {payload.route.title}
          </Text>
        );
      }}
      // renderIcon={(payload) => { return <Image source={{ uri: payload.route.image }} style={{ width: 40,height: 40 }} /> }}
    />
  );
  render() {
    return (
      <>
        <Container>
          <HeaderC
            official
            input={inputSearch => {
              this.setState({inputSearch});
            }}
          />
          <Content>
            <TabView
              navigationState={this.state}
              renderScene={SceneMap({
                doiVoucher: DoiVoucher,
                doiSanPham: DoiSanPham,
              })}
              onIndexChange={index => this.setState({index})}
              initialLayout={{width}}
              renderTabBar={this._renderHeader}
              onIndexChange={this._handleIndexChange}
            />
          </Content>
        </Container>
      </>
    );
  }
}
