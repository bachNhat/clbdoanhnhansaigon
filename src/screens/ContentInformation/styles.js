import {StyleSheet, Dimensions} from 'react-native';
import {colors} from '../../constants/theme';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    padding: 5,
    width,
    height,
    // backgroundColor: '#dddddd',
  },
  image: {
    width: '100%',
    height: 200,
    borderRadius: 8,
  },
  title: {
    fontSize: 18,
    // paddingVertical: 10,
    fontWeight: '700',
    marginTop: 10,
  },
  lead: {
    fontWeight: '700',
    marginBottom: 5,
  },
  time: {
    color: colors.PRIMARY,
  },
  iconTime: {
    fontSize: 20,
    marginRight: 10,
    color: colors.PRIMARY,
  },
});

export default styles;
