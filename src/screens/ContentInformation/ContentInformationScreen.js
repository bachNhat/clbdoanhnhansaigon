import React, {useState} from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput,
  Dimensions,
  ScrollView,
} from 'react-native';
import Block from '../../components/block';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Picker,
  Form,
} from 'native-base';
import HeaderC from '../../components/headerC';
import styles from './styles';
import images from '../../constants/images';
import {colors} from '../../constants/theme';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';
import moment from 'moment';

const ContentInformationScreen = ({navigation} = this.props) => {
  const pram = navigation.getParam('item', '');
  console.log(pram);
  return (
    <Container>
      <HeaderC title="CHI TIẾT" back />
      <Content style={styles.content} contentContainerStyle={{flex: 1}}>
        <ScrollView>
          <Image
            source={{uri: pram.image} || images.LOGO_DNSG}
            style={styles.image}
            resizeMode="contain"
          />
          <Block middle style={{width: '100%'}}>
            <Text style={styles.title}>{pram.name}</Text>
            <Block row center style={{paddingVertical: 5}}>
              <Icon name="time" style={styles.iconTime} />
              <Text style={styles.time}>
                {moment(pram.updatedAt).format('L')}
              </Text>
            </Block>
            <Text style={styles.lead}>{pram.lead}</Text>
            <HTMLView value={pram.contents} stylesheet={styles} />
          </Block>
          <Block style={{height: 50}} />
        </ScrollView>
      </Content>
    </Container>
  );
};

export default ContentInformationScreen;
