import AsyncStorage from '@react-native-community/async-storage';
import React, {Component} from 'react';

export const storeData = async (keyStore, value) => {
  try {
    await AsyncStorage.setItem(keyStore, value);
  } catch (e) {
    console.log(e);
  }
};

export const getData = async keyStore => {
  try {
    const value = await AsyncStorage.getItem(keyStore);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    console.log(e);
  }
};
